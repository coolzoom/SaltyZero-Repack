# SaltyZero's Repack
基于AZ 2.0.0 由咸蛋发布 
Based on Azerothcore v2.0.0 and authorized by saltyzero@foxmail.com

说明 
Description:

我致力于打造一款高拓展性和高性能的服务器, 可使用简单接口实现. 现在已经支持各种外部dll动态链接库格式的脚本. 所有文件仅端部分77兆. 
I'm intended to build one high expansibility and performance server with easy game APIs. Now, it has supported for various scripts by using external dll. All files are about 77MB.

新版本 
Upcoming:

在1.3版本中会添加一些可玩性的功能. 
1. v1.3 will add some playable functions.


特性 
Features:

使用C++的接口重写脚本逻辑而不使用lua, 参考已有的幻化和世界聊天功能. 
1. Use game APIs with C++ rather than Eluna to override your own scripts just like it in demo project [transmog] or [world_chat].

更简洁的文件结构. 
2. More brief file structure.

动态热加载卸载dll动态链接库, 使用gm命令即可完成. 
3. Dynamic hot loading or freeing scrit dll with GM cmd .script load xxx / .script free xxx. It's a so powerful function.

使用命令行脚本一键创建新工程, 并且支持一键编译所有脚本, 我使用的是VS2017 SDK版本17134, 可根据需要自己修改. 
4. Use the cmd [create_new_script] to create a new scirpt project with the default configuration [RelWithDebInfo|Win32]. Now you can build all script with cmd [build_all].

Requirements:

1. If you play at localhost just requiring [mysql] and one mysql database browser just like [navicat] or [heidisql].

2. If you open it on the internet and that will requirie one host tool just like [apache].

Steps:

1. Open the mysql service.

2. Import all sql within data folder and other script sql if you use it.

3. Set your server conf to connect to the mysql on it's port with default 3306.

4. Create a character and have fun.

Note:
1. Compile the script project with configuration [RelWithDefInfo]. 

2. The script name and the exported function name XXX matches the form of XXX.dll and AddSC_XXX() { new XXX(); } which will be filled in Database.

3. Game APIs are not complete but useful I'm trying to update.

4. You can debug your scripts in the debugger directly as long as you set your debugger configuration as the same as the demo script [transmog] or [world_chat].

5. If you find any compiling error or any good suggestion just contact me.
