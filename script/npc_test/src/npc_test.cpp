#include "npc_test.h" 
 
#define GOSSIP_SUSURRUS         "I am your daddy."

class npc_test : public CreatureScript
{
public:
	npc_test() : CreatureScript("npc_test") { }

	bool OnGossipHello(Player* player, Creature* creature)
	{
		player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, GOSSIP_SUSURRUS, GOSSIP_SENDER_MAIN, GOSSIP_ACTION_INFO_DEF);
		player->SEND_GOSSIP_MENU(player->GetGossipTextId(creature), creature->GetGUID());

		return true;
	}

	bool OnGossipSelect(Player* player, Creature*  /*creature*/, uint32 /*sender*/, uint32 action)
	{
		player->PlayerTalkClass->ClearMenus();
		player->CLOSE_GOSSIP_MENU();

		return true;
	}
};

_module void FreeSC_npc_test() { ScriptRegistry<CreatureScript>::FreeScript("npc_test"); }
_module void AddSC_npc_test(bool reload) { new npc_test(); if (reload) {} }
