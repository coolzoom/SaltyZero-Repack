@echo off
chcp 65001

:: authorized by saltyzero@foxmail.com
:: VS141(VS2017) RelWithDebInfo Win32

echo please input a script name or * for all scripts
set /p name=

if "%name%"=="*" (
for /R %~dp0 %%i in (%name%.vcxproj) do (
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\MSBuild.exe" %%i /t:Rebuild /p:Configuration=RelWithDebInfo /p:Platform=Win32
)
) else (
"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\MSBuild\15.0\Bin\MSBuild.exe" %name%\%name%.vcxproj /t:Rebuild /p:Configuration=RelWithDebInfo /p:Platform=Win32
)


:Exit
Pause