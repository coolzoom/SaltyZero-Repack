// 默认编码utf-8 

#ifndef _CUSTOMDATABASE_H
#define _CUSTOMDATABASE_H

#include "DatabaseWorkerPool.h"
#include "MySQLConnection.h"

class CustomDatabaseConnection : public MySQLConnection
{
public:
	//- Constructors for sync and async connections
	CustomDatabaseConnection(MySQLConnectionInfo& connInfo) : MySQLConnection(connInfo) { }
	CustomDatabaseConnection(ACE_Activation_Queue* q, MySQLConnectionInfo& connInfo) : MySQLConnection(q, connInfo) { }

	//- Loads database type specific prepared statements
	void DoPrepareStatements() {};
};

typedef DatabaseWorkerPool<CustomDatabaseConnection> CustomDatabaseWorkerPool;


#endif