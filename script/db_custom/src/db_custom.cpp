// 默认编码utf-8

#include "Configuration/config.h"
#include "db_custom.h"

CustomDatabaseWorkerPool CustomDatabase;

extern "C" int APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	UNREFERENCED_PARAMETER(lpReserved);
	switch (dwReason)
	{
		case DLL_PROCESS_ATTACH:
		{
			sLog->outString("                  Load [db_custom]\t[0x%x]\tsuccess!", hInstance);

			uint8 async_threads, synch_threads;

			std::string dbstring = "127.0.0.1;3306;root;al1223;custom";
			async_threads = 1; // uint8(sConfigMgr->GetIntDefault("WorldDatabase.WorkerThreads", 1));
			synch_threads = 1; // uint8(sConfigMgr->GetIntDefault("WorldDatabase.SynchThreads", 1));

			///- Initialise the world database
			if (!CustomDatabase.Open(dbstring, async_threads, synch_threads))
			{
				sLog->outError("Cannot connect to custom database %s", dbstring.c_str());
				return false;
			}

			QueryResult result = CustomDatabase.Query("SELECT * FROM global");

			if (result)
			{
				do
				{
					Field* fields = result->Fetch();
					uint32 key = fields[0].GetValue<uint32>();
					std::string value = fields[1].GetValue<std::string>();
					uint8 type = fields[2].GetValue<uint8>();

					sLog->outString("%d %s", key, value.c_str());

				} while (result->NextRow());


				// some logic....
			}
		} break;

		case DLL_PROCESS_DETACH:
		{

		} break;
	};

	return 1;
}

